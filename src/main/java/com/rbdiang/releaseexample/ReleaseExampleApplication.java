package com.rbdiang.releaseexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReleaseExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReleaseExampleApplication.class, args);
	}

}
